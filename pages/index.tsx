import { AboutSection } from '../components/AboutSection/AboutSection'
import { ContactSection } from '../components/ContactSection/ContactSection'
import { FooterSection } from '../components/FooterSection/FooterSection'
import { Header } from '../components/Header/Header'
import { HeroSection } from '../components/HeroSection/HeroSection'
import { MaterialSection } from '../components/MaterialSection/MaterialSection'
import { ProductSection } from '../components/ProductSection/ProductSection'
import { ServiceSection } from '../components/ServiceSection/ServiceSection'
import { Modal } from '../components/Modal/Modal'
import styles from '../styles/Home.module.css'
import { useState } from 'react'


export default function Home() {

  const [showModal, setShowModal] = useState(false)

  return (
    <>
      <main className={styles.main}>
        {showModal && <Modal setShowModal={setShowModal}/>}
        <Header
          setShowModal={setShowModal}
        />
        <HeroSection className={styles.main__hero} id="hero"/>
        <AboutSection className={styles.main__about} id="about"/>
        <ServiceSection className={styles.main_service} id="services"/>
        <ProductSection className={styles.main__product} id="materials"/>
        <MaterialSection className={styles.main__material}/>
        <ContactSection className={styles.main__contact}/>
        <FooterSection className={styles.main__footer}/>
      </main>
    </>
  ) 
}
