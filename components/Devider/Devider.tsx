import styles from "./Devider.module.scss"
import { FC, PropsWithChildren } from 'react';

export const Devider: FC<PropsWithChildren> = ({
    children
}) => {
    return(
        <span className={styles.text}>
            {children}
        </span>
    )
}