import { FC } from 'react';
import classNames from 'classnames'
import { Input } from '../Input/Input'
import styles from './FooterSection.module.scss'

interface IProps {
    className: string
}


export const FooterSection: FC<IProps> = ({
    className
}) => {
    return (
        <div className={classNames(styles.footerSection, className)}>
            <div className={styles.footerSection__row}>
                <div className={styles.footerSection__rowMain}>
                    <img src="/images/logo.svg" alt="" className={styles.footerSection__rowMainImage} />
                    <p className={styles.footerSection__rowMainTitle}>One of the best furniture agency.</p>
                </div>
                <div className={styles.footerSection__contact}>
                    <p className={styles.footerSection__contactText}>Enter  your email to get the latest news</p>
                    
                    <Input
                        type="email"
                        placeholder="Email Address"
                        link="/images/arrow.svg"
                    />

                </div>
            </div>
            <div className={styles.footerSection__socials}>
                <span>Follow us On</span>
                <div className={styles.footerSection__socialsGroup}>
                    <img src="/images/facebook.svg" alt="" className={styles.footerSection__socialsGroupImg}/>
                    <img src="/images/inst.svg" alt="" className={styles.footerSection__socialsGroupImg}/>
                    <img src="/images/tik-tok.svg" alt="" className={styles.footerSection__socialsGroupImg}/>
                    <img src="/images/youtube.svg" alt="" className={styles.footerSection__socialsGroupImg}/>
                </div>
            </div>
        </div>
    )
}