import classNames from "classnames"
import styles from "./HeroSection.module.scss"
import { FC } from 'react';

interface IProps {
    className: string,
    id: string
}

export const HeroSection: FC<IProps> = ({
    className,
    id
}) => {
    
    return (
        <div className={classNames(styles.hero, className)} id={id}>
            <h1 className={styles.hero__title}>Design your interor with high quality.</h1>
            <div className={styles.hero__stats}>
                <div>
                    <span>350+</span>
                    <span>Project Completed</span>
                </div>
                <div>
                    <span>23+</span>
                    <span>Professional Teams</span>
                </div>
                <div>
                    <span>15+</span>
                    <span>Years Experience</span>
                </div>
            </div>
            <img src="/images/section-image-1.png" alt="" className={styles.hero__img}/>
        </div>
    )

}