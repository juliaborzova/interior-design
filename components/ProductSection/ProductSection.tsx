import classNames from "classnames"
import { Devider } from "../Devider/Devider"
import { ProductLink } from "./components/ProductLink/ProductLink"
import { links } from "./constants"
import styles from "./ProductSection.module.scss"
import { FC } from 'react';

interface IProps {
    className: string,
    id: string
}

export const ProductSection: FC<IProps> = ({
    className,
    id
}) => {
    return (
        <div className={classNames(className, styles.ProductSection)} id={id}>
            <Devider>PRODUCT</Devider>
            <div className={styles.ProductSection__text}>
                <p className={styles.ProductSection__textTitle}>Choose your product themes.</p>
                <p className={styles.ProductSection__textInfo}>Find the theme you want. If our choice of theme is not what you want, you can customize it as you want.</p>
            </div>
            <div className={styles.ProductSection__Links}>
                {links.map((item, index) => {
                    return (
                        <ProductLink
                            key={index}
                            number={item.number}
                            title={item.title}
                            text={item.text}
                        />
                    )
                })}
            </div>

        </div>
    )
}