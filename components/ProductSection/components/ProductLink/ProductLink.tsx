import styles from "./ProductLink.module.scss"
import { FC } from 'react';

interface IProps {
    number: string,
    title: string,
    text: string
}

export const ProductLink: FC<IProps> = ({
    number,
    title,
    text
}) => {
    return (
        <div className={styles.Link}>
            <div className={styles.Link__item}>
                <span className={styles.Link__itemNumber}>{number}</span>
                <div className={styles.Link__itemInfo}>
                    <span className={styles.Link__itemInfoTitle}>{title}</span>
                    <p className={styles.Link__itemInfoText}>{text}</p>
                </div>
            </div>
            <img src="/images/arrow-right.svg" alt="" className={styles.Link__Img} />
        </div>
    )

}