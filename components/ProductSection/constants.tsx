export const links = [
    {
        number: "01",
        title: "Vintage",
        text: "The use of simple and limited elements to get the best effect or impression."
    },
    {
        number: "02",
        title: "Minimalist",
        text: "The use of simple and limited elements to get the best effect or impression."
    },
    {
        number: "03",
        title: "Modern",
        text: "The use of simple and limited elements to get the best effect or impression."
    },
    {
        number: "04",
        title: "Transitional",
        text: "The use of simple and limited elements to get the best effect or impression."
    }
]