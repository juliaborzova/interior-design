import { Button } from "../Button/Button";
import styles from "./Header.module.scss";
import { FC } from 'react';

interface IProps {
    setShowModal: (newValue: boolean) => void
}

export const Header: FC<IProps> = ({
    setShowModal
}) => {

    const handleShowModal = () => {
        setShowModal(true)
    }

    return (
        <div className={styles.header}>
            <img src="/images/logo.svg" alt="" className={styles.header__logo} />

            <div className={styles.header__links}>

                <span>
                    <a href="#hero">
                        Home
                    </a>
                </span>
                <span>
                    <a href="#about">
                        About Us
                    </a>
                </span>
                <span>
                    <a href="#services">
                        Services
                    </a>
                </span>
                <span>
                    <a href="#materials">
                        Our Teams
                    </a>
                </span>

                <Button
                    className={styles.header__linksItem}
                    onClick={handleShowModal}
                >
                    Contact Us
                </Button>
            </div>
        </div>
    )
}