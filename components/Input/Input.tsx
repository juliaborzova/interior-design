import { FC } from 'react';
import styles from './Input.module.scss'

interface IProps {
    type: 'email' | 'text' | 'checkbox';
    link?: string;
    placeholder: string;
}

export const Input: FC<IProps> = ({
    type,
    link,
    placeholder
}) => {
    return (
        <div className={styles.input}>
            <input
                type={type}
                className={styles.input__field}
                placeholder={placeholder}
            />
            {link && <img src={link} alt="" className={styles.input__img} />}
        </div>
    )

}