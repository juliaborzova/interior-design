import styles from './Modal.module.scss'
import { FC } from 'react';
import { Input } from '../Input/Input';
import { Button } from "../Button/Button"

interface IProps {
    setShowModal: (newValue: boolean) => void
}

export const Modal: FC<IProps> = ({
    setShowModal
}) => {

    const handleShowModal = () => {
        setShowModal(false)
    }

    return (
        <div className={styles.wrapper} onClick={handleShowModal}>
            <div className={styles.modal}>
                <span className={styles.modal__title}>Contact us</span>
                <div className={styles.modal__form}>
                    <Input
                        type='text'
                        placeholder='Your name'
                    />
                    <Input
                        type='email'
                        placeholder='Email Address'
                    />
                </div>
                <Button className={styles.modal__button}>Contact me</Button>
            </div>
        </div>

    )
}