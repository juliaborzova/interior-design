import classNames from "classnames"
import { Button } from "../Button/Button"
import { Devider } from "../Devider/Devider"
import styles from "./AboutSection.module.scss"
import { FC } from 'react';

interface IProps {
    className: string,
    id: string
}

export const AboutSection: FC<IProps> = ({
    className,
    id
}) => {
    return (
        <div className={classNames(styles.main, className)} id={id}>
            <Devider>ABOUT</Devider>
            <p className={styles.main__text}>
                “We are one of the best furniture agency. Prioritizing customers and
                making purchases easy are the hallmarks of our agency.”
            </p>
            <div className={styles.main__author}>
                <img src="/images/avatar.png" alt="" />
                <div className={styles.main__authorName}>
                    <span>Arga Danaan</span>
                    <span>CEO of Dananz</span>
                </div>
            </div>
            <div className={styles.main__block}>
                <img src="/images/section-image-2.png" alt="" className={styles.main__blockImg}/>
                <div className={styles.main__blockDescription}>
                    <p>
                        Online learning with us does not interfere with your daily life.
                        Because learning can be done anytime and anywhere.
                    </p>
                    <Button className={styles.main__blockDescriptionButton}>Learn More</Button>
                </div>
            </div>
        </div>
    )
}