import classNames from "classnames"
import { Devider } from "../Devider/Devider"
import { ServiceLink } from "./components/ServiceLink/ServiceLink"
import { links } from "./constants"
import styles from "./ServiceSection.module.scss"
import { FC } from 'react';

interface IProps {
    className: string,
    id: string
}

export const ServiceSection: FC<IProps> = ({
    className,
    id
}) => {
    return (
        <div className={classNames(className, styles.serviceSection)} id={id}>
            <div className={styles.serviceSection__card}>
                <Devider>SERVICE</Devider>
                <span className={styles.serviceSection__cardTitle}>attractive furniture with the best quality.</span>
                <p className={styles.serviceSection__cardText}>
                    Customize your interior design into a dream place with the best designers and quality furniture. We try our best to fulfill your expectations.
                </p>
                <div className={styles.serviceSection__cardLinks}>

                    {links.map((item, index) => {
                        return (
                            <ServiceLink
                                key={index}
                                number={item.number}
                                title={item.title}
                            />
                        )
                    })}
                </div>
            </div>
            <img className={styles.serviceSection__image} src="/images/section-image-3.png" alt="" />
        </div>
    )
}