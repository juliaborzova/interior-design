import styles from "./ServiceLink.module.scss"
import { FC } from 'react';

interface IProps {
    number: string,
    title: string
}

export const ServiceLink = ({
    number,
    title
}) => {
    return (
        <div className={styles.serviceLink}>
            <div className={styles.serviceLink__info}>
                <span className={styles.serviceLink__infoNumber}>
                    {number}
                </span>
                <span className={styles.serviceLink__infoText}>
                    {title}
                </span>
            </div>
            <img src="/images/arrow-right.svg" className={styles.serviceLink__img} alt="" />
        </div>
    )
}