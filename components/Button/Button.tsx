import classNames from "classnames"
import styles from "./Button.module.scss"
import { FC, PropsWithChildren } from 'react';

interface IProps {
    className: string
    onClick?: () => void
}

export const Button: FC<PropsWithChildren<IProps>> = ({
    className,
    onClick,
    children
}) => {

    return (
        <button className={classNames(styles.button, className)} onClick={onClick}>
            {children}
        </button>
    )

}