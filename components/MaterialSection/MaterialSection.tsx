import classNames from 'classnames'
import { Button } from '../Button/Button'
import { Devider } from '../Devider/Devider'
import styles from './MaterialSection.module.scss'
import { FC } from 'react';

interface IProps {
    className: string,
}

export const MaterialSection: FC<IProps> = ({
    className
}) => {
    return (
        <div className={classNames(styles.MaterialSection, className)}>
            <div className={styles.MaterialSection__info}>
                <Devider>MATERIAL</Devider>
                <p className={styles.MaterialSection__infoTitle}>Choice of materials for quality furniture.</p>
                <p className={styles.MaterialSection__infoText}>You can custom the material as desired. And our furniture uses the best materials and selected quality materials.</p>
                <Button className={styles.MaterialSection__infoButton}>See Materials</Button>
            </div>
            <div className={styles.MaterialSection__images}>
                <img src="/images/section-image-4.png" alt="" className={styles.MaterialSection__imagesFirst}/>
                <div className={styles.MaterialSection__imagesGroup}>
                    <img src="/images/section-image-5.png" alt="" className={styles.MaterialSection__imagesGroupSecond}/>
                    <img src="/images/section-image-6.png" alt="" className={styles.MaterialSection__imagesGroupThird}/>
                </div>
            </div>
        </div>
    )
}