import classNames from 'classnames'
import { Button } from '../Button/Button'
import styles from './ContactSection.module.scss'
import { FC } from 'react';

interface IProps {
    className: string,
}

export const ContactSection: FC<IProps> = ({
    className
}) => {
    return(
        <div className={classNames(styles.contactSection, className)}>
            <p className={styles.contactSection__title}>Lets discuss making your interior like a dream place!</p>
            <div className={styles.contactSection__block}>
                <p className={styles.contactSection__blockText}>Contact us below to work together to build your amazing interior</p>
                <Button className={styles.contactSection__blockButton}>Contact Us</Button>
            </div>
        </div>
    )
}